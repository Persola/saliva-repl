export default {
  titan: {
    classes: [
      'same-line',
      'bubble-even',
      'titan',
    ],
    childPresnos: [
      'name',
      'child',
    ],
  },
  olympian: {
    classes: [
      'same-line',
      'bubble-even',
      'olympian',
    ],
    childPresnos: [
      'name',
      'child',
    ],
  },
};
