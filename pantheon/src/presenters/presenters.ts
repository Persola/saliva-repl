import presentTitan from './present-titan';
import presentOlympian from './present-olympian';

export default {
  titan: presentTitan,
  olympian: presentOlympian,
};
