import webview from 'saliva-repl-vscode/dist/webview';
import salivaRendererIntegration from 'saliva/built/saliva-builtin-renderer-integration';

webview(salivaRendererIntegration);
