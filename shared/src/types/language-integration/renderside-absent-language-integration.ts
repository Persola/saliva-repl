export type RendersideAbsentLanguageIntegration = {
  id: null;
  keyToNewSynoAttrs: null;
  renderers: null;
  styles: null;
};
