import type { childPresnoInstruction } from './child-presno-instruction';

export type RendererAttrs = {
  classes: string[];
  childPresnos: childPresnoInstruction[];
};
