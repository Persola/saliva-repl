export type CoresideAbsentLanguageIntegration = {
  id: null;
  grammar: null;
  primitives: null;
  keyToNewSynoAttrs: null;
  interpret: null;
  synoValidators: null;
  presenters: null;
};
