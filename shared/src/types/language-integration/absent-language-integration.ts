export type AbsentLanguageIntegration = {
  id: null;
  grammar: null;
  primitives: null;
  keyToNewSynoAttrs: null;
  interpret: null;
  synoValidators: null;
  presenters: null;
  renderers: null;
  styles: null;
};
