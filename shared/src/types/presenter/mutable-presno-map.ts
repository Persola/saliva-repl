import type { Presno } from './presno';
import type { SynoId } from '../syntactic/syno-id';

export type MutablePresnoMap = Record<SynoId, Presno>;
