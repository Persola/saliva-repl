export type Edge = {
  readonly key: string;
  readonly index: (number | null);
};
