export type GraphValidationResult = {
  valid: boolean;
  messages: string[];
};
