import type { NamePartPresAttrs } from '../presenter/presno-attrs/name-part-attrs';

export type NamePartProps = {
  readonly presno: NamePartPresAttrs;
};
