import type { SynoRef } from './syntactic/syno-ref';
import type { NamePartRef } from './name-part-ref';

export type ChildPresnoRef = (SynoRef | NamePartRef);
