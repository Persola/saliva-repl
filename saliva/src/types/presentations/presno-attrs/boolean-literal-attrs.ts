export type BooleanLiteralPresAttrs = {
  readonly syntype: 'booleanLiteral';
  readonly value: boolean;
};
