import type { CoreSynoAttrs } from 'saliva-repl/dist/types/syntactic/core-syno-attrs';
import type { FunctionParameterAttrs } from '../syno-attrs/function-parameter-attrs';

export type MutableFunctionParameter = CoreSynoAttrs & FunctionParameterAttrs;
