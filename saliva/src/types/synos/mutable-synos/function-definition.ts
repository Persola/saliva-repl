import type { CoreSynoAttrs } from 'saliva-repl/dist/types/syntactic/core-syno-attrs';
import type { FunctionDefinitionAttrs } from '../syno-attrs/function-definition-attrs';

export type MutableFunctionDefinition = CoreSynoAttrs & FunctionDefinitionAttrs;
