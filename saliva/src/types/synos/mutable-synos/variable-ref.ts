import type { CoreSynoAttrs } from 'saliva-repl/dist/types/syntactic/core-syno-attrs';
import type { VariableRefAttrs } from '../syno-attrs/variable-ref-attrs';

export type MutableVariableRef = CoreSynoAttrs & VariableRefAttrs;
