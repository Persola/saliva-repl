import type { CoreSynoAttrs } from 'saliva-repl/dist/types/syntactic/core-syno-attrs';
import type { BooleanLiteralAttrs } from '../syno-attrs/boolean-literal-attrs';

export type MutableBooleanLiteral = CoreSynoAttrs & BooleanLiteralAttrs;
