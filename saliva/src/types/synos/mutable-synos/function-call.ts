import type { CoreSynoAttrs } from 'saliva-repl/dist/types/syntactic/core-syno-attrs';
import type { FunctionCallAttrs } from '../syno-attrs/function-call-attrs';

export type MutableFunctionCall = CoreSynoAttrs & FunctionCallAttrs;
