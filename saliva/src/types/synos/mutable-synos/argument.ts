import type { CoreSynoAttrs } from 'saliva-repl/dist/types/syntactic/core-syno-attrs';
import type { ArgumentAttrs } from '../syno-attrs/argument-attrs';

export type MutableArgument = CoreSynoAttrs & ArgumentAttrs;
