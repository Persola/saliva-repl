export default {
  classes: [
    'same-line',
    'leaf',
    'bubble-even',
    'boolean-literal',
  ],
  childPresnos: [
    { attr: 'value', as: 'string' },
  ],
};
