export default {
  classes: [
    'same-line',
    'bubble-even',
    'argument',
  ],
  childPresnos: [
    'name',
    'value',
  ],
};
