export default {
  classes: [
    'same-line',
    'leaf',
    'bubble-even',
    'function-parameter',
  ],
  childPresnos: [
    'slot',
  ],
};
