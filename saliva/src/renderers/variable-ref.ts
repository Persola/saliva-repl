export default {
  classes: [
    'same-line',
    'leaf',
    'bubble-even',
    'variable-ref',
  ],
  childPresnos: [
    'name',
  ],
};
