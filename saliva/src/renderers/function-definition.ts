export default {
  classes: [
    'function-definition',
  ],
  childPresnos: [
    'name',
    'parameters',
    'body',
  ],
};
