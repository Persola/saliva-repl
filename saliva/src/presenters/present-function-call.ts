import type { StateSelector } from 'saliva-repl/dist/types/state-selector';
import type { PresentAndReturnRef } from 'saliva-repl/dist/types/presenter/present-and-return-ref';
import type { PresnoRef } from 'saliva-repl/dist/types/presenter/presno-ref';
import type { Syno } from 'saliva-repl/dist/types/syntactic/syno';

// @ts-ignore how do I configure TS to ignore webpacked imports?
import primitives from '../primitives.yml';

import type { FunctionCall } from '../types/synos/function-call';
import type { FunctionDefinition } from '../types/synos/function-definition';
import type { FunctionCallPresAttrs } from '../types/presentations/presno-attrs/function-call-attrs';

const primitiveIds = Object.keys(primitives);

export default (
  funkshunCall: FunctionCall,
  state: StateSelector,
  presentAndReturnRef: PresentAndReturnRef,
): FunctionCallPresAttrs => {
  let name: (null | PresnoRef) = null;
  let callee: (null | PresnoRef) = null;
  let resolved = false;

  if (funkshunCall.callee) {
    const calleeSyno: Syno = state.getSyno(funkshunCall.callee.id);

    if (calleeSyno.syntype !== 'functionDefinition') {
      throw new Error('new type?');
    }

    const calleeFuncDef = calleeSyno as FunctionDefinition;
    resolved = true;

    if (primitiveIds.includes(calleeFuncDef.id)) {
      name = presentAndReturnRef(
        {
          valid: true,
          presnoIndex: 0,
          prestype: 'NamePart',
          text: calleeFuncDef.name,
        },
        funkshunCall,
      );
    }

    if (funkshunCall.callee.relation === 'child') {
      callee = presentAndReturnRef(funkshunCall.callee);
    }
  }

  return {
    syntype: 'functionCall',
    name,
    argumentz: funkshunCall.argumentz.map(argRef => presentAndReturnRef(argRef)),
    callee,
    resolved,
  };
};
